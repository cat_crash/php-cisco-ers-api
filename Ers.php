<?php
namespace cat_crash\ers;


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;

  
Class Ers {
	
	public $baseApiUri='https://10.17.1.1:9060/ers/config/';
	public $username=null;
	public $password=null;
	public $verbose=true;

	private $client;


	public function __construct(){

		$stack = HandlerStack::create();
		$stack->push(
		    Middleware::log(
		        new Logger('Logger'),
		        new MessageFormatter('{uri} - {code} - {req_body} - {res_body}')
		    )
		);


		$this->client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => $this->baseApiUri,
		    // You can set any number of default request options.
		    'timeout'  => 10.0,
		    'handler' => ($this->verbose)?$stack:null,
		    'verify'=>false
		]);

	}


	public function getSponsorPortal($id){
		$headers=[
			"Accept"=>"application/json",
			"Content-type"=>"application/json"
		];
	
		$response=$this->client->request('GET', 'sponsorportal/'.$id,[
			'auth' => [$this->username, $this->password],
			'headers'=>$headers,
		]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		} else {
			$object=json_decode($response->getBody()->getContents(),true);
			return $object;
		}
	}


	public function getAllSponsorPorals(){
		$headers=[
			"Accept"=>"application/json",
			"Content-type"=>"application/json"
		];
	
		$response=$this->client->request('GET', 'sponsorportal',[
			'auth' => [$this->username, $this->password],
			'headers'=>$headers,
		]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		} else {
			$return=[];
			$object=json_decode($response->getBody()->getContents(),true);

			if(!empty($object) && array_key_exists('SearchResult', $object) && array_key_exists('resources', $object['SearchResult'])){

				foreach($object['SearchResult']['resources'] as $resource){
					$return[]=new SponsorPortal($resource);
				}

			}
			return $return;
		}

	}


	public function getAllLocations(){
		$headers=[
			"Accept"=>"application/json",
			"Content-type"=>"application/json"
		];
	
		$response=$this->client->request('GET', 'guestlocation',[
			'auth' => [$this->username, $this->password],
			'headers'=>$headers,
		]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		} else {
			$return=[];
			$object=json_decode($response->getBody()->getContents(),true);
			if(!empty($object) && array_key_exists('SearchResult', $object) && array_key_exists('resources', $object['SearchResult'])){

				foreach($object['SearchResult']['resources'] as $resource){
					$return[]=new Location($resource);
				}

			}

			return $object;
		}

	}


	public function createGuestUser(GuestUser $user){

		$payload=[
			'GuestUser'=>$user->asArray()
		];
		
		$headers=[
			"Accept"=>"application/json",
			"Content-type"=>"application/json"
		];
	
		$response=$this->client->request('POST', 'guestuser',[
			'auth' => [$this->username, $this->password],
			'headers'=>$headers,
			'body' => json_encode($payload,JSON_FORCE_OBJECT)
		]);
		
		if( $response->getStatusCode()!==201){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		} else {
			return true;
		}
		
	}


	public function __set($name, $value) 
    {
		$functionName='set'.$name;
        if(method_exists($this,'set'.$name) && property_exists($this, $name)){
			call_user_func(array($this, $functionName),$value);
		}else {
			throw new \Exception('Setting unknown variable: '.$name);
		}
    }

    public function __get($name) {
		$functionName='get'.$name;
		if(method_exists($this,'get'.$name) && property_exists($this, 'get'.$name)){
			return call_user_func(array($this, $functionName));
		} else {
			throw new \Exception('Getting unknown variable: '.$name);
		}
	}

}

?>