<?php
namespace cat_crash\ers;

  
Class GuestInfo {
	
	private $userName;
	private $password;
	private $firstName;
	private $lastName;
	private $emailAddress;
	private $enabled;
	private $notificationLanguage;
	private $smsServiceProvider;

	private function setsmsServiceProvider($smsServiceProvider){
		$this->smsServiceProvider=$smsServiceProvider;
	}

	public function getsmsServiceProvider(){
		return $this->smsServiceProvider;
	}

	private function setnotificationLanguage($notificationLanguage){
		$this->notificationLanguage=$notificationLanguage;
	}

	public function getnotificationLanguage(){
		return $this->notificationLanguage;
	}

	private function setenabled($enabled){
		$this->enabled=$enabled;
	}

	public function getenabled(){
		return $this->enabled;
	}

	private function setemailAddress($emailAddress){
		$this->emailAddress=$emailAddress;
	}

	public function getemailAddress(){
		return $this->emailAddress;
	}

	private function setlastName($lastName){
		$this->lastName=$lastName;
	}

	public function getlastName(){
		return $this->lastName;
	}

	private function setfirstName($firstName){
		$this->firstName=$firstName;
	}

	public function getfirstName(){
		return $this->firstName;
	}

	private function setpassword($password){
		$this->password=$password;
	}

	public function getpassword(){
		return $this->password;
	}

	private function setuserName($userName){
		$this->userName=$userName;
	}

	public function getuserName(){
		return $this->userName;
	}


	public function asArray($object=null){
		$return=[];
		if($object==null){
			$object=$this;
		}
		$reflect = new \ReflectionClass($object);
		$props   = $reflect->getProperties();

		foreach ($props as $prop) {
			$name=$prop->getName();
			if(!empty($name) && method_exists($this,'get'.$name)){
		    	$return[$name]=call_user_func([$this,'get'.$name]);
		    }
		}

		return $return;

	}
	

	public function __construct($settings=null){

		if(is_array($settings) && count($settings)>0){
			while(list($key,$value)=each($settings)){
				if(!empty($key)){
					$functionName='set'.$key;
					call_user_func(array($this, $functionName),$value);
				}
			}
		}
		
	}
	
	public function __set($name, $value) 
    {
		$functionName='set'.$name;
        if(method_exists($this,'set'.$name) && property_exists($this, 'set'.$name)){
			call_user_func(array($this, $functionName),$value);
		}else {
			throw new \Exception('Setting unknown variable: '.$name);
		}
    }

    public function __get($name) {
		$functionName='get'.$name;
		if(method_exists($this,'get'.$name) && property_exists($this, 'get'.$name)){
			return call_user_func(array($this, $functionName));
		} else {
			throw new \Exception('Getting unknown variable: '.$name);
		}
	}

}

?>