<?php
namespace cat_crash\ers;

use Ramsey\Uuid\Uuid;

  
Class GuestUser {
	
	private $name;
	private $id;
	private $description;
	private $customFields=[];
	private $guestType;
	private $status;
	private $reasonForVisit;
	private $personBeingVisited;
	private $sponsorUserName;
	private $sponsorUserId;
	private $statusReason;
	private $portalId;
	private $guestAccessInfo;
	private $guestInfo;
	

	public function __construct($settings=null){

		if(is_array($settings) && count($settings)>0){
			while(list($key,$value)=each($settings)){
				if(!empty($key)){
					$functionName='set'.$key;
					call_user_func(array($this, $functionName),$value);
				}
			}
		}
	
		if(!$this->id){
			$this->id=Uuid::uuid4()->toString();
		}

	}

	private function setGuestInfo(GuestInfo $GuestInfo){
		$this->guestInfo=$GuestInfo->asArray();
	}

	public function getGuestInfo(){
		return $this->guestInfo;
	}

	private function setguestAccessInfo(guestAccessInfo $guestAccessInfo){
		$this->guestAccessInfo=$guestAccessInfo->asArray();
	}

	public function getguestAccessInfo(){
		return $this->guestAccessInfo;
	}

	private function setportalId($portalId){
		$this->portalId=$portalId;
	}

	public function getportalId(){
		return $this->portalId;
	}

	private function setstatusReason($statusReason){
		$this->statusReason=$statusReason;
	}

	public function getstatusReason(){
		return $this->statusReason;
	}

	private function setsponsorUserId($sponsorUserId){
		$this->sponsorUserId=$sponsorUserId;
	}

	public function getsponsorUserId(){
		return $this->sponsorUserId;
	}

	private function setsponsorUserName($sponsorUserName){
		$this->sponsorUserName=$sponsorUserName;
	}

	public function getsponsorUserName(){
		return $this->sponsorUserName;
	}

	private function setpersonBeingVisited($personBeingVisited){
		$this->personBeingVisited=$personBeingVisited;
	}

	public function getpersonBeingVisited(){
		return $this->personBeingVisited;
	}

	private function setreasonForVisit($reasonForVisit){
		$this->reasonForVisit=$reasonForVisit;
	}

	public function getreasonForVisit(){
		return $this->reasonForVisit;
	}
	

	private function setstatus($status){
		$this->status=$status;
	}

	public function getstatus(){
		return $this->status;
	}

	private function setguestType($guestType){
		$this->guestType=$guestType;
	}


	public function getguestType(){
		return $this->guestType;
	}

	private function setcustomFields($fields=[]){
		if(!empty($fields) && is_array($fields)){
			$this->customFields=$fields;
		}
	}

	public function getcustomFields(){
		return $this->customFields;
	}

	private function setname($name){
		$this->name=$name;
	}
	
	public function getname(){
		return $this->name;
	}
	
	private function setdescription($description){
		$this->description=$description;
	}
	
	public function getdescription(){
		return $this->description;
	}
	
	private function setid($id=null){
		$this->id=$id;
	}
	
	public function getid(){
		return $this->id;
	}

	public function getGuestUser(){
		return $this;
	}

	public function asArray($object=null){
		$return=[];
		if($object==null){
			$object=$this;
		}
		$reflect = new \ReflectionClass($object);
		$props   = $reflect->getProperties();

		foreach ($props as $prop) {
			$name=$prop->getName();
			if(!empty($name) && method_exists($this,'get'.$name)){
		    	$return[$name]=call_user_func([$this,'get'.$name]);
		    }
		}

		return $return;

	}
	
	public function __set($name, $value) 
    {
		$functionName='set'.$name;
        if(method_exists($this,'set'.$name) && property_exists($this, $name)){
			call_user_func(array($this, $functionName),$value);
		}else {
			throw new \Exception('Setting unknown variable: '.$name);
		}
    }

    public function __get($name) {
		$functionName='get'.$name;
		if(method_exists($this,'get'.$name) && property_exists($this, 'get'.$name)){
			return call_user_func(array($this, $functionName));
		} else {
			throw new \Exception('Getting unknown variable: '.$name);
		}
	}

}

?>