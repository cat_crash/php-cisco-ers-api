<?php
namespace cat_crash\ers;
  
Class SponsorPortal {
	
	private $id;
	private $name;
	private $description;
	

	public function __construct($settings=null){

		if(is_array($settings) && count($settings)>0){
			while(list($key,$value)=each($settings)){
				if(!empty($key)){
					$functionName='set'.$key;
					if(method_exists($this,'set'.$key) && property_exists($this, $key)){
						call_user_func(array($this, $functionName),$value);
					}
				}
			}
		}

	}

	private function setname($name){
		$this->name=$name;
	}
	
	public function getname(){
		return $this->name;
	}
	
	private function setdescription($description){
		$this->description=$description;
	}
	
	public function getdescription(){
		return $this->description;
	}
	
	private function setid($id=null){
		$this->id=$id;
	}
	
	public function getid(){
		return $this->id;
	}

	public function asArray($object=null){
		$return=[];
		if($object==null){
			$object=$this;
		}
		$reflect = new \ReflectionClass($object);
		$props   = $reflect->getProperties();

		foreach ($props as $prop) {
			$name=$prop->getName();
			if(!empty($name) && method_exists($this,'get'.$name)){
		    	$return[$name]=call_user_func([$this,'get'.$name]);
		    }
		}

		return $return;

	}
	
	public function __set($name, $value) 
    {
		$functionName='set'.$name;
        if(method_exists($this,'set'.$name) && property_exists($this, $name)){
			call_user_func(array($this, $functionName),$value);
		}else {
			throw new \Exception('Setting unknown variable: '.$name);
		}
    }

    public function __get($name) {
		$functionName='get'.$name;
		if(method_exists($this,'get'.$name) && property_exists($this, 'get'.$name)){
			return call_user_func(array($this, $functionName));
		} else {
			throw new \Exception('Getting unknown variable: '.$name);
		}
	}

}

?>