<?php
namespace cat_crash\ers;
  
Class guestAccessInfo {
	
	private $validDays;
	private $fromDate;
	private $toDate;
	private $location;
	private $ssid;
	private $groupTag;
	
	
	
	public function getvalidDays(){
		return $this->validDays;
	}
	
	public function getfromDate(){
		return $this->fromDate;
	}
	
	public function gettoDate(){
		return $this->toDate;
	}
	
	public function getlocation(){
		return $this->location;
	}
	
	public function getssid(){
		return  $this->ssid;
	}
	
	public function getgroupTag(){
		return $this->groupTag;
	}


	private function setvalidDays($validDays){
		$this->validDays=$validDays;
	}
	
	private function setfromDate($fromDate){
		$datetime=\DateTime::createFromFormat('m/j/Y', $fromDate);
		$this->fromDate=$datetime->format('m/j/Y H:i');
	}
	
	private function settoDate($toDate=null){
		if(!$toDate){
			$datetime=\DateTime::createFromFormat('m/j/Y H:i', $this->fromDate);
			$datetime->add(new \DateInterval('P'.($this->validDays-1).'D'));
		}
		$this->toDate=$datetime->format('m/j/Y H:i');
	}
	
	private function setlocation($location){
		$this->location=$location;
	}
	
	private function setssid($ssid){
		$this->ssid=$ssid;
	}
	
	private function setgroupTag($groupTag){
		$this->groupTag=$groupTag;
	}
	
	public function asArray($object=null){
		$return=[];
		if($object==null){
			$object=$this;
		}
		$reflect = new \ReflectionClass($object);
		$props   = $reflect->getProperties();

		foreach ($props as $prop) {
			$name=$prop->getName();
			if(!empty($name) && method_exists($this,'get'.$name)){
		    	$return[$name]=call_user_func([$this,'get'.$name]);
		    }
		}

		return $return;

	}

	public function __construct($settings=null){

		if(is_array($settings) && count($settings)>0){
			while(list($key,$value)=each($settings)){
				if(!empty($key)){
					$functionName='set'.$key;
					call_user_func(array($this, $functionName),$value);
				}
			}
		}
	}

	
	public function __set($name, $value) 
    {
		$functionName='set'.$name;
        if(method_exists($this,'set'.$name) && property_exists($this, 'set'.$name)){
			call_user_func(array($this, $functionName),$value);
		}else {
			throw new \Exception('Setting unknown variable: '.$name);
		}
    }

    public function __get($name) {
		$functionName='get'.$name;
		if(method_exists($this,'get'.$name) && property_exists($this, 'get'.$name)){
			return call_user_func(array($this, $functionName));
		} else {
			throw new \Exception('Getting unknown variable: '.$name);
		}
	}
    

}

?>